/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file util_test.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <vector>
#include <complex>
#include <map>
#include <type_traits>
#include <sstream>
#include <iostream>

#include <tulib/util/function_wrapper.hpp>
#include <tulib/util/input_definitions.hpp>
#include <tulib/util/io.hpp>

namespace tulib::test
{

namespace util::detail
{
//! Test class
class A
   :  private std::vector<double>
{
   public:
      double mf0() const
      {
         return 42.0;
      }

      int mf1(int i) const
      {
         return i;
      }

      std::complex<float> mf2(float r, float i) const noexcept
      {
         return std::complex<float>(r, i);
      }

      bool operator()(double d_) const { return d_ > 2.0; }
};

void f1(int& i)
{
   i=0;
}

enum class testKeyType : int
{  FIRST = 0
,  SECOND
,  THIRD
,  FOURTH
};

} /* namespace util::detail */

/**
 * Test the function_wrapper class
 **/
struct function_wrapper_test
{
   static void run()
   {
      // Lambda function as reference
      auto lambda = [](int i) { return i+2; };
      auto wrapped_lambda_ref = tulib::util::wrap_function(lambda);
      static_assert(std::is_same_v<decltype(wrapped_lambda_ref), tulib::util::detail::function_wrapper_impl<decltype(lambda), true>>, "lambda is not wrapped as reference");
      CHECK(wrapped_lambda_ref(2) == 4);

      // Lambda function as value
      auto wrapped_lambda_val = tulib::util::wrap_function([](double x) -> double { return 2.0*x; });
      static_assert(std::is_same_v<decltype(wrapped_lambda_val), tulib::util::detail::function_wrapper_impl<typename decltype(wrapped_lambda_val)::function_type, false>>, "lambda is not wrapped as value");
      CHECK(wrapped_lambda_val(3.0) == Approx(6.0).epsilon(std::numeric_limits<double>::epsilon() * 2));

      // Function pointer
      auto wrapped_fptr = tulib::util::wrap_function(&util::detail::f1);
      static_assert(std::is_same_v<decltype(wrapped_fptr), tulib::util::detail::function_wrapper_impl<decltype(&util::detail::f1), false>>, "f1 is not wrapped as value");
      int i = 1;
      wrapped_fptr(i);
      CHECK(i == 0);

      // Functor as reference
      util::detail::A a;
      auto wrapped_functor_ref = tulib::util::wrap_function(a);
      static_assert(std::is_same_v<decltype(wrapped_functor_ref), tulib::util::detail::function_wrapper_impl<util::detail::A, true>>, "functor is not wrapped as reference");
      CHECK(wrapped_functor_ref(2.5));

      // Move instead
      auto wrapped_functor_val = tulib::util::wrap_function(std::move(a));
      static_assert(std::is_same_v<decltype(wrapped_functor_val), tulib::util::detail::function_wrapper_impl<util::detail::A, false>>, "functor is not wrapped as value");
      CHECK(wrapped_functor_val(2.5));
   }
};

/**
 * Test input_definitions class
 **/
struct input_definitions_test
{
   static void run()
   {
      tulib::util::input_definitions<util::detail::testKeyType> def;

      // Add and get values
      def.add(util::detail::testKeyType::FIRST, 4.5);
      def.add(util::detail::testKeyType::SECOND, 1);
      def.add(util::detail::testKeyType::THIRD, std::string("third"));
      auto x1f = def.get<float>(util::detail::testKeyType::FIRST);
      static_assert(std::is_same_v<decltype(x1f), float>, "Expected float");
      CHECK(x1f == Approx(4.5).epsilon(std::numeric_limits<float>::epsilon() * 2));
      auto x1d = def.get<double>(util::detail::testKeyType::FIRST);
      static_assert(std::is_same_v<decltype(x1d), double>, "Expected double");
      CHECK(x1d == Approx(4.5).epsilon(std::numeric_limits<double>::epsilon() * 2));
      auto x2 = def.get<int>(util::detail::testKeyType::SECOND);
      CHECK(x2 == 1);
      auto x3 = def.get<std::string>(util::detail::testKeyType::THIRD);
      CHECK(x3 == "third");

      // Try validation
      typename decltype(def)::defaults_map_type<std::string> defaults;
      defaults[util::detail::testKeyType::THIRD] =
      {  {  "third"
         ,  {  {util::detail::testKeyType::FIRST, 2.3}
            ,  {util::detail::testKeyType::FOURTH, 7}
            }
         }
      ,  {  "third2"
         ,  {  {util::detail::testKeyType::SECOND, 9}
            ,  {util::detail::testKeyType::FOURTH, 3}
            }
         }
      };
      CHECK(def.validate(defaults));
      CHECK(def.get<int>(util::detail::testKeyType::FOURTH) == 7);
      CHECK(def.get<double>(util::detail::testKeyType::FIRST) == Approx(4.5).epsilon(std::numeric_limits<double>::epsilon() * 2));
   }
};

/**
 * Test io methods and classes
 **/
struct io_test
{
   static void run()
   {
      std::ostringstream oss5, oss8;
      tulib::io::writer_collection wc({{oss5, 5}, {oss8, 8}});

      wc << "Test1";
      CHECK(oss5.str() == "Test1");
      CHECK(oss8.str() == "Test1");
      {
         tulib::io::SCOPE_IOLEVEL(wc, 6);
         wc << "Test2";
      }
      CHECK(oss5.str() == "Test1");
      CHECK(oss8.str() == "Test1Test2");
      wc << "Test3";
      CHECK(oss5.str() == "Test1Test3");
      CHECK(oss8.str() == "Test1Test2Test3");

      std::ostringstream oss;
      tulib::io::writer_collection wc2(oss, 2);
      {
         tulib::io::SCOPE_INDENT(wc2);
         wc2 << "Test";
      }
      CHECK(oss.str() == "   Test");
      wc2 << "Test2";
      CHECK(oss.str() == "   TestTest2");
   }
};


} /* namespace tulib::test */
