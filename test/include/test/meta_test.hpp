/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file meta_test.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <vector>
#include <complex>
#include <map>
#include <type_traits>

#include <tulib/meta.hpp>

namespace tulib::test
{

namespace detail
{
//! Test class
class A
   :  private std::vector<double>
{
   public:
      double mf0() const
      {
         return 42.0;
      }

      int mf1(int i) const
      {
         return i;
      }

      std::complex<float> mf2(float r, float i) const noexcept
      {
         return std::complex<float>(r, i);
      }
};

//! Test functions
A f0()
{
   return A();
}
void f1(int& i)
{
   i=0;
}
std::vector<long> f2(int* ip, bool b)
{
   return b ? std::vector<long>() : std::vector<long>{ ip[0], ip[1] };
}

}

/**
 * Test the template argument classes
 **/
struct template_argument_test
{
   static void run()
   {
      // Check is_template
      constexpr bool complex_double_is_template = tulib::meta::is_template_v<std::complex<double>>;
      static_assert(complex_double_is_template, "std::complex<double> is not recognized as template");
      constexpr bool vec_double_is_template = tulib::meta::is_template_v<std::vector<double>>;
      static_assert(vec_double_is_template, "std::vector<double> is not recognized as template");
      constexpr bool vec_complex_double_is_template = tulib::meta::is_template_v<std::vector<std::complex<double>>>;
      static_assert(vec_complex_double_is_template, "std::vector<std::complex<double>> is not recognized as template");
      constexpr bool map_is_template = tulib::meta::is_template_v<std::map<int, double>>;
      static_assert(map_is_template, "std::map<int, double> is not recognized as template");
      constexpr bool A_is_template = tulib::meta::is_template_v<detail::A>;
      static_assert(!A_is_template, "A (private std::vector<double>) is recognized as template");

      // Check template_argument
      using vec_double_arg = tulib::meta::template_argument_t<std::vector<double>>;
      constexpr bool vda_check = std::is_same_v<vec_double_arg, double>;
      static_assert(vda_check, "std::vector<double>: wrong template_argument");
      using vec_complex_double_arg = tulib::meta::template_argument_t<std::vector<std::complex<double>>>;
      constexpr bool vcda_check = std::is_same_v<vec_complex_double_arg, std::complex<double>>;
      static_assert(vcda_check, "std::vector<std::complex<double>>: wrong template_argument");
      using vec_vec_double_arg = tulib::meta::template_argument_t<std::vector<std::vector<double>>>;
      constexpr bool vvda_check = std::is_same_v<vec_vec_double_arg, std::vector<double>>;
      static_assert(vvda_check, "std::vector<std::vector<double>>: wrong template_argument");

      // Check template_argument_or_complex
      using complex_double_arg_or_c = tulib::meta::template_argument_or_complex_t<std::complex<double>>;
      constexpr bool cdaoc_check = std::is_same_v<complex_double_arg_or_c, std::complex<double>>;
      static_assert(cdaoc_check, "td::complex<double>: wrong template_argument_or_complex");
      using vec_double_arg_or_c = tulib::meta::template_argument_or_complex_t<std::vector<double>>;
      constexpr bool vdaoc_check = std::is_same_v<vec_double_arg_or_c, double>;
      static_assert(vdaoc_check, "std::vector<double>: wrong template_argument_or_complex");
      using vec_complex_double_arg_or_c = tulib::meta::template_argument_or_complex_t<std::vector<std::complex<double>>>;
      constexpr bool vcdaoc_check = std::is_same_v<vec_complex_double_arg_or_c, std::complex<double>>;
      static_assert(vcdaoc_check, "std::vector<std::complex<double>>: wrong template_argument_or_complex");

      // Check recursive_template_argument
      using vec_vec_double_arg_rec = tulib::meta::recursive_template_argument_t<std::vector<std::vector<double>>>;
      constexpr bool vvdar_check = std::is_same_v<vec_vec_double_arg_rec, double>;
      static_assert(vvdar_check, "std::vector<std::vector<double>>: wrong recursive_template_argument");
      using vec_complex_double_arg_rec = tulib::meta::recursive_template_argument_t<std::vector<std::complex<double>>>;
      constexpr bool vcdar_check = std::is_same_v<vec_complex_double_arg_rec, double>;
      static_assert(vcdar_check, "std::vector<std::complex<double>>: wrong recursive_template_argument");

      // Check recursive_template_argument_or_complex
      using vec_vec_double_arg_rec_or_c = tulib::meta::recursive_template_argument_or_complex_t<std::vector<std::vector<double>>>;
      constexpr bool vvdaroc_check = std::is_same_v<vec_vec_double_arg_rec_or_c, double>;
      static_assert(vvdaroc_check, "std::vector<std::vector<double>>: wrong recursive_template_argument_or_complex");
      using vec_complex_double_arg_rec_or_c = tulib::meta::recursive_template_argument_or_complex_t<std::vector<std::complex<double>>>;
      constexpr bool vcdaroc_check = std::is_same_v<vec_complex_double_arg_rec_or_c, std::complex<double>>;
      static_assert(vcdaroc_check, "std::vector<std::complex<double>>: wrong recursive_template_argument_or_complex");
   }
};

/**
 * Test function_traits
 **/
struct function_traits_test
{
   static void run()
   {
      // Functions
      constexpr size_t ac_f0 = tulib::meta::argument_count<decltype(&detail::f0)>;
      using ret_f0_t = tulib::meta::return_type<decltype(&detail::f0)>;
      static_assert(ac_f0 == 0, "f0 should take 0 arguments");
      static_assert(std::is_same_v<ret_f0_t, detail::A>, "Wrong return-type deduction for f0");

      constexpr size_t ac_f1 = tulib::meta::argument_count<decltype(&detail::f1)>;
      using ret_f1_t = tulib::meta::return_type<decltype(&detail::f1)>;
      using arg0_f1_t = tulib::meta::argument_type<0, decltype(&detail::f1)>;
      static_assert(ac_f1 == 1, "f1 should take 1 argument");
      static_assert(std::is_same_v<ret_f1_t, void>, "Wrong return-type deduction for f1");
      static_assert(std::is_same_v<arg0_f1_t, int&>, "Wrong argument-type deduction for f1");

      constexpr size_t ac_f2 = tulib::meta::argument_count<decltype(&detail::f2)>;
      using ret_f2_t = tulib::meta::return_type<decltype(&detail::f2)>;
      using arg0_f2_t = tulib::meta::argument_type<0, decltype(&detail::f2)>;
      using arg1_f2_t = tulib::meta::argument_type<1, decltype(&detail::f2)>;
      static_assert(ac_f2 == 2, "f2 should take 2 arguments");
      static_assert(std::is_same_v<ret_f2_t, std::vector<long>>, "Wrong return-type deduction for f2");
      static_assert(std::is_same_v<arg0_f2_t, int*>, "Wrong argument-type deduction for f2 (arg0)");
      static_assert(std::is_same_v<arg1_f2_t, bool>, "Wrong argument-type deduction for f2 (arg1)");

      // Member functions
      constexpr size_t ac_mf0 = tulib::meta::argument_count<decltype(&detail::A::mf0)>;
      using ret_mf0_t = tulib::meta::return_type<decltype(&detail::A::mf0)>;
      static_assert(ac_mf0 == 0, "A::mf0 should take 0 arguments");
      static_assert(std::is_same_v<ret_mf0_t, double>, "Wrong return-type deduction for A::f0");

      constexpr size_t ac_mf1 = tulib::meta::argument_count<decltype(&detail::A::mf1)>;
      using ret_mf1_t = tulib::meta::return_type<decltype(&detail::A::mf1)>;
      using arg0_mf1_t = tulib::meta::argument_type<0, decltype(&detail::A::mf1)>;
      static_assert(ac_mf1 == 1, "A::mf1 should take 1 argument");
      static_assert(std::is_same_v<ret_mf1_t, int>, "Wrong return-type deduction for A::f1");
      static_assert(std::is_same_v<arg0_mf1_t, int>, "Wrong argument-type deduction for A::f1");

      constexpr size_t ac_mf2 = tulib::meta::argument_count<decltype(&detail::A::mf2)>;
      using ret_mf2_t = tulib::meta::return_type<decltype(&detail::A::mf2)>;
      using arg0_mf2_t = tulib::meta::argument_type<0, decltype(&detail::A::mf2)>;
      using arg1_mf2_t = tulib::meta::argument_type<1, decltype(&detail::A::mf2)>;
      static_assert(ac_mf2 == 2, "A::f2 should take 2 arguments");
      static_assert(std::is_same_v<ret_mf2_t, std::complex<float>>, "Wrong return-type deduction for A::f2");
      static_assert(std::is_same_v<arg0_mf2_t, float>, "Wrong argument-type deduction for A::mf2 (arg0)");
      static_assert(std::is_same_v<arg1_mf2_t, float>, "Wrong argument-type deduction for A::mf2 (arg1)");

      // Lambda function
      auto lambda = [](int i, double d) -> bool { return std::pow(d, i) > 0.0; };
      constexpr size_t ac_l = tulib::meta::argument_count<decltype(lambda)>;
      using ret_l_t = tulib::meta::return_type<decltype(lambda)>;
      using arg0_l_t = tulib::meta::argument_type<0, decltype(lambda)>;
      using arg1_l_t = tulib::meta::argument_type<1, decltype(lambda)>;
      using l_func_t = tulib::meta::function_t<decltype(lambda)>;
      static_assert(ac_l == 2, "lambda should take 2 arguments");
      static_assert(std::is_same_v<ret_l_t, bool>, "Wrong return-type deduction for lambda");
      static_assert(std::is_same_v<arg0_l_t, int>, "Wrong argument-type deduction for lambda (arg0)");
      static_assert(std::is_same_v<arg1_l_t, double>, "Wrong argument-type deduction for lambda (arg1)");
      static_assert(std::is_same_v<l_func_t, std::function<bool(int, double)>>, "Wrong deduction of function type for lambda inside class.");

      class test
      {
      public:
         int internal(int i) const
         {
            return -i;
         }
         double d = 42.0;

         test()
         {
            auto l = [](test* t_, int i_) { return t_->internal(i_); };
            using type = tulib::meta::function_t<decltype(l)>;
            static_assert(std::is_same_v<type, std::function<int(test*, int)>>, "Function type deduction failed for lambda in class");
         }
      };
   }
};

} /* namespace tulib::test */
