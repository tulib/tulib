/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file math_test.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <iostream>
#include <array>

#include <tulib/math.hpp>

#include <test/test_utils.hpp>

namespace tulib::test::detail
{
struct vector_double_private;
double norm2(const vector_double_private&);
void scale(vector_double_private&, double);
std::size_t size(const vector_double_private& v);
double dot_product(const vector_double_private&, const vector_double_private&);
void axpy(vector_double_private&, double, const vector_double_private&);
double scaled_error2(const vector_double_private&, const vector_double_private&, const vector_double_private&, double, double, bool);
} /* namespace tulib::test::detail */

namespace tulib::test
{
namespace detail
{
class vector_double : public std::vector<double> { public: vector_double(long s) : std::vector<double>(s) {}; };

struct vector_double_private : private std::vector<double>
{
   public:
      friend double norm2(const vector_double_private&);
      friend void scale(vector_double_private&, double);
      friend std::size_t size(const vector_double_private&);
      friend double dot_product(const vector_double_private&, const vector_double_private&);
      friend void axpy(vector_double_private&, double, const vector_double_private&);
      friend double scaled_error2(const vector_double_private&, const vector_double_private&, const vector_double_private&, double, double, bool);
      vector_double_private(long s) : std::vector<double>(s) {};

      template<typename T>
      void load_in(const T& v)
      {
         std::copy(v.begin(), v.end(), this->begin());
      }
};
double norm2(const vector_double_private& v)
{
//   std::cout   << " CALLING friend norm2 for vector_double_private" << std::endl;
   double result = 0;
   for(const auto& e : v)
   {
      result += e*e;
   }
   return result;
}
void scale(vector_double_private& v, double s)
{
   for(auto& e : v)
   {
      e *= s;
   }
}
std::size_t size(const vector_double_private& v)
{
   return v.size();
}
double dot_product(const vector_double_private& a, const vector_double_private& b)
{
   assert(a.size() == b.size());

   double dot = 0;
   for(size_t i=0; i<a.size(); ++i)
   {
      dot += tulib::math::conj(a[i]) * b[i];
   }

   return dot;
}
void axpy(vector_double_private& a, double s, const vector_double_private& b)
{
   assert(a.size() == b.size());
   for(size_t i=0; i<a.size(); ++i)
   {
      a[i] += s * b[i];
   }
}
double scaled_error2(const vector_double_private& dy, const vector_double_private& yn, const vector_double_private& yo, double abs, double rel, bool maxerr)
{
   double err2 = 0;
   double tmp = 0;
   double sc = 0;
   for(size_t i=0; i<dy.size(); ++i)
   {
      sc = abs + rel*std::max(std::abs(yn[i]), std::abs(yo[i]));
      tmp = tulib::math::abs2(dy[i] /sc);
      if (  maxerr
         )
      {
         err2 = std::max(err2, tmp);
      }
      else
      {
         err2 += tmp;
      }
   }
   return err2;
}

template<typename T> struct five_array : public std::array<T, 5> {};
} /* namespace detail */


/**
 * Test the tulib::math::wrap_norm2 implementation
 **/
struct container_norm2_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      // Try wrapper for tulib::vector (even and odd size to test tiling)
      const auto vec_even = tulib::test::make_random_vector<T>(100);
      real_t ref_norm2_even = 0;
      for(const auto& e : vec_even)
      {
         ref_norm2_even += tulib::math::abs2(e);
      }
      const auto wrap_norm2_even = tulib::math::wrap_norm2(vec_even);
      CHECK(ref_norm2_even == Approx(wrap_norm2_even).epsilon(10*eps)); // "tulib::math::wrap_norm2(tulib::vector) (even size) failed!");

      const auto vec_odd = tulib::test::make_random_vector<T>(101);
      real_t ref_norm2_odd = 0;
      for(const auto& e : vec_odd)
      {
         ref_norm2_odd += tulib::math::abs2(e);
      }
      const auto wrap_norm2_odd = tulib::math::wrap_norm2(vec_odd);
      CHECK(ref_norm2_odd == Approx(wrap_norm2_odd).epsilon(eps*10)); // "tulib::math::wrap_norm2(tulib::vector) (odd size) failed!");

      // Create vector of vectors with same data
      std::vector<std::vector<T>> vv(20);
      size_t count=0;
      for(auto& v : vv)
      {
         v.resize(5);
         for(auto& elem : v)
         {
            elem = vec_even[count++];
         }
      }
      const auto vv_norm2 = tulib::math::wrap_norm2(vv);
      CHECK(ref_norm2_even == Approx(vv_norm2).epsilon(eps*10)); // "tulib::math::wrap_norm2(vector<vector<>>) failed!");

      // Create array of vectors
      detail::five_array<std::vector<T> > fatv  =  {  std::vector<T>(vec_even.begin(), vec_even.begin()+20)
                                                   ,  std::vector<T>(vec_even.begin()+20, vec_even.begin()+40)
                                                   ,  std::vector<T>(vec_even.begin()+40, vec_even.begin()+60)
                                                   ,  std::vector<T>(vec_even.begin()+60, vec_even.begin()+80)
                                                   ,  std::vector<T>(vec_even.begin()+80, vec_even.begin()+100)
                                                   };
      const auto fatv_norm2 = tulib::math::wrap_norm2(fatv);
      CHECK(ref_norm2_even == Approx(fatv_norm2).epsilon(eps*10)); // "tulib::math::wrap_norm2(std::array<std::vector<T>,5>) failed!");

      // Create tuple of scalar, vector, and matrix
      T ts = vec_odd[0];
      std::vector<T> tv(36);
      for(size_t i=0; i<tv.size(); ++i)
      {
         tv[i] = vec_odd[i+1];
      }
      std::vector<std::vector<T>> tm(8);
      std::for_each(tm.begin(), tm.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm.size(); ++icol)
      {
         for(size_t irow=0; irow<tm[icol].size(); ++irow)
         {
            tm[irow][icol] = vec_odd[count+37];
            ++count;
         }
      }
      auto tup = std::make_tuple(std::move(ts), std::move(tv), std::move(tm));
      const auto tup_norm2 = tulib::math::wrap_norm2(tup);
      CHECK(ref_norm2_odd == Approx(tup_norm2).epsilon(eps*10)); // "tulib::math::wrap_norm2(std::tuple<scalar, vector, vector<vector>>) failed!");

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double vd(vec_even.size());
         std::copy(vec_even.begin(), vec_even.end(), vd.begin());
         auto vd_norm2 = tulib::math::wrap_norm2(vd);
         CHECK(vd_norm2 == Approx(ref_norm2_even).epsilon(eps*10)); // "tulib::math::wrap_norm2 for non-template type failed!");

         detail::vector_double_private vdp(vec_even.size());
         vdp.load_in(vec_even);
         constexpr bool vdp_has_norm2_func_impl = tulib::meta::is_detected_v<tulib::math::detail::has_norm2_func_type, detail::vector_double_private>;
         REQUIRE(vdp_has_norm2_func_impl); // "norm2(vector_double_private) not found");
         auto vdp_norm2 = tulib::math::wrap_norm2(vdp);
         CHECK(vdp_norm2 == Approx(ref_norm2_even).epsilon(eps*10)); // "tulib::math::wrap_norm2 for non-template type with friend norm2() failed!");
      }
   }
};

/**
 * Test the tulib::math::wrap_scale implementation
 **/
struct container_scale_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      // Try wrapper for tulib::vector
      const T rand_scalar = tulib::test::make_random_scalar<T>();
      const auto a = tulib::test::make_random_vector<T>(100);
      const real_t a_norm = tulib::math::wrap_norm(a);

      auto b = a;
      tulib::math::wrap_scale(b, rand_scalar);
      const real_t b_norm = tulib::math::wrap_norm(b);
      CHECK(b_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale(tulib::vector) failed!");

      // Create vector of vectors with same data
      std::vector<std::vector<T>> c(20);
      size_t count=0;
      for(auto& v : c)
      {
         v.resize(5);
         for(auto& elem : v)
         {
            elem = a[count++];
         }
      }
      tulib::math::wrap_scale(c, rand_scalar);
      const real_t c_norm = tulib::math::wrap_norm(c);
      CHECK(c_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale(vector<vector<>>) failed!");

      // Create array of vectors
      detail::five_array<std::vector<T> > fatv  =  {  std::vector<T>(a.begin(), a.begin()+20)
                                                   ,  std::vector<T>(a.begin()+20, a.begin()+40)
                                                   ,  std::vector<T>(a.begin()+40, a.begin()+60)
                                                   ,  std::vector<T>(a.begin()+60, a.begin()+80)
                                                   ,  std::vector<T>(a.begin()+80, a.begin()+100)
                                                   };
      tulib::math::wrap_scale(fatv, rand_scalar);
      const auto fatv_norm = tulib::math::wrap_norm(fatv);
      CHECK(fatv_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale(std::array<std::vector<T>,5>) failed!");

      // Create tuple of scalar, vector, and matrix
      T ts = a[0];
      std::vector<T> tv(35);
      for(size_t i=0; i<tv.size(); ++i)
      {
         tv[i] = a[i+1];
      }
      std::vector<std::vector<T>> tm(8);
      std::for_each(tm.begin(), tm.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm.size(); ++icol)
      {
         for(size_t irow=0; irow<tm[icol].size(); ++irow)
         {
            tm[irow][icol] = a[count+36];
            ++count;
         }
      }
      auto tup = std::make_tuple(std::move(ts), std::move(tv), std::move(tm));
      tulib::math::wrap_scale(tup, rand_scalar);
      const real_t tup_norm = tulib::math::wrap_norm(tup);
      CHECK(tup_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale(std::tuple) failed!");

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double vd(a.size());
         std::copy(a.begin(), a.end(), vd.begin());
         tulib::math::wrap_scale(vd, rand_scalar);
         auto vd_norm = tulib::math::wrap_norm(vd);
         CHECK(vd_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale for non-template type failed!");

         detail::vector_double_private vdp(a.size());
         vdp.load_in(a);
         constexpr bool vdp_has_scale_func_impl = tulib::meta::is_detected_v<tulib::math::detail::has_scale_func_type, detail::vector_double_private, double>;
         REQUIRE(vdp_has_scale_func_impl); // "scale(vector_double_private) not found");
         tulib::math::wrap_scale(vdp, rand_scalar);
         auto vdp_norm = tulib::math::wrap_norm(vdp);
         CHECK(vdp_norm == Approx(a_norm*std::abs(rand_scalar)).epsilon(eps*10)); // "tulib::math::wrap_scale for non-template type with friend norm2() failed!");
      }
   }
};

/**
 * Test the tulib::math::wrap_size implementation
 **/
struct container_size_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      const std::size_t vsize = 100;

      const auto vec = tulib::test::make_random_vector<T>(vsize);
      auto vec_size = tulib::math::wrap_size(vec);
      CHECK(vec_size == vsize);

      std::vector<std::vector<T>> vv(20);
      for(auto& v : vv)
      {
         v.resize(5);
      }
      const auto vv_size = tulib::math::wrap_size(vv);
      CHECK(vv_size == vsize);

      // Create tuple of scalar, vector, and matrix
      T ts = 0.0;
      std::vector<T> tv(35);
      std::vector<std::vector<T>> tm(8);
      std::for_each(tm.begin(), tm.end(), [](auto& x_) { x_.resize(8); });
      auto tup = std::make_tuple(std::move(ts), std::move(tv), std::move(tm));
      const auto tup_size = tulib::math::wrap_size(tup);
      CHECK(tup_size == vsize);

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double vd(vsize);
         const auto vd_size = tulib::math::wrap_size(vd);
         CHECK(vd_size == vsize);

         detail::vector_double_private vdp(vsize);
         const auto vdp_size = tulib::math::wrap_size(vdp);
         CHECK(vdp_size == vsize);
      }
   }
};

/**
 * Test the tulib::math::wrap_dot_product implementation
 **/
struct container_dot_product_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      // Try wrapper for tulib::vector (even and odd size to test tiling)
      const auto vec_even_a = tulib::test::make_random_vector<T>(100);
      const auto vec_even_b = tulib::test::make_random_vector<T>(100);
      T ref_dot_even = 0;
      for(size_t i=0; i<vec_even_a.size(); ++i)
      {
         ref_dot_even += tulib::math::conj(vec_even_a[i]) * vec_even_b[i];
      }
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_even_a, vec_even_b), ref_dot_even, eps*100));
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_even_a, vec_even_a), T(tulib::math::wrap_norm2(vec_even_a)), eps*10));
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_even_b, vec_even_b), T(tulib::math::wrap_norm2(vec_even_b)), eps*10));

      const auto vec_odd_a = tulib::test::make_random_vector<T>(101);
      const auto vec_odd_b = tulib::test::make_random_vector<T>(101);
      T ref_dot_odd = 0;
      for(size_t i=0; i<vec_odd_a.size(); ++i)
      {
         ref_dot_odd += tulib::math::conj(vec_odd_a[i]) * vec_odd_b[i];
      }
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_odd_a, vec_odd_b), ref_dot_odd, eps*100));
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_odd_a, vec_odd_a), T(tulib::math::wrap_norm2(vec_odd_a)), eps*10));
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vec_odd_b, vec_odd_b), T(tulib::math::wrap_norm2(vec_odd_b)), eps*10));

      // Create vector of vectors with same data
      std::vector<std::vector<T>> vv_a(20);
      std::vector<std::vector<T>> vv_b(20);
      size_t count=0;
      for(size_t i=0; i<vv_a.size(); ++i)
      {
         vv_a[i].resize(5);
         vv_b[i].resize(5);
         for(size_t j=0; j<vv_a[i].size(); ++j)
         {
            vv_a[i][j] = vec_even_a[count];
            vv_b[i][j] = vec_even_b[count];
            ++count;
         }
      }
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(vv_a, vv_b), ref_dot_even, eps*100));

      // Create array of vectors
      detail::five_array<std::vector<T> > fatv_a  =   {  std::vector<T>(vec_even_a.begin(),    vec_even_a.begin()+20)
                                                      ,  std::vector<T>(vec_even_a.begin()+20, vec_even_a.begin()+40)
                                                      ,  std::vector<T>(vec_even_a.begin()+40, vec_even_a.begin()+60)
                                                      ,  std::vector<T>(vec_even_a.begin()+60, vec_even_a.begin()+80)
                                                      ,  std::vector<T>(vec_even_a.begin()+80, vec_even_a.begin()+100)
                                                      };
      detail::five_array<std::vector<T> > fatv_b  =   {  std::vector<T>(vec_even_b.begin(),    vec_even_b.begin()+20)
                                                      ,  std::vector<T>(vec_even_b.begin()+20, vec_even_b.begin()+40)
                                                      ,  std::vector<T>(vec_even_b.begin()+40, vec_even_b.begin()+60)
                                                      ,  std::vector<T>(vec_even_b.begin()+60, vec_even_b.begin()+80)
                                                      ,  std::vector<T>(vec_even_b.begin()+80, vec_even_b.begin()+100)
                                                      };
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(fatv_a, fatv_b), ref_dot_even, eps*100));

      // Create tuple of scalar, vector, and matrix
      T ts_a = vec_odd_a[0];
      std::vector<T> tv_a(36);
      for(size_t i=0; i<tv_a.size(); ++i)
      {
         tv_a[i] = vec_odd_a[i+1];
      }
      std::vector<std::vector<T>> tm_a(8);
      std::for_each(tm_a.begin(), tm_a.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm_a.size(); ++icol)
      {
         for(size_t irow=0; irow<tm_a[icol].size(); ++irow)
         {
            tm_a[irow][icol] = vec_odd_a[count+37];
            ++count;
         }
      }
      auto tup_a = std::make_tuple(std::move(ts_a), std::move(tv_a), std::move(tm_a));
      T ts_b = vec_odd_b[0];
      std::vector<T> tv_b(36);
      for(size_t i=0; i<tv_b.size(); ++i)
      {
         tv_b[i] = vec_odd_b[i+1];
      }
      std::vector<std::vector<T>> tm_b(8);
      std::for_each(tm_b.begin(), tm_b.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm_b.size(); ++icol)
      {
         for(size_t irow=0; irow<tm_b[icol].size(); ++irow)
         {
            tm_b[irow][icol] = vec_odd_b[count+37];
            ++count;
         }
      }
      auto tup_b = std::make_tuple(std::move(ts_b), std::move(tv_b), std::move(tm_b));
      CHECK(tulib::test::check_complex_equal(tulib::math::wrap_dot_product(tup_a, tup_b), ref_dot_odd, eps*100));

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double vd_a(vec_even_a.size());
         std::copy(vec_even_a.begin(), vec_even_a.end(), vd_a.begin());
         detail::vector_double vd_b(vec_even_b.size());
         std::copy(vec_even_b.begin(), vec_even_b.end(), vd_b.begin());
         CHECK(tulib::math::wrap_dot_product(vd_a, vd_b) == Approx(ref_dot_even).epsilon(eps*100));

         detail::vector_double_private vdp_a(vec_even_a.size());
         vdp_a.load_in(vec_even_a);
         detail::vector_double_private vdp_b(vec_even_b.size());
         vdp_b.load_in(vec_even_b);
         CHECK(tulib::math::wrap_dot_product(vdp_a, vdp_b) == Approx(ref_dot_even).epsilon(eps*100));
      }
   }
};

/**
 * Test the tulib::math::wrap_axpy implementation
 **/
struct container_axpy_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      const T rand_scalar = tulib::test::make_random_scalar<T>();
      const auto abs2_s = tulib::math::abs2(rand_scalar);

      // Try wrapper for tulib::vector
      const auto vec_a = tulib::test::make_random_vector<T>(100);
      const auto vec_b = tulib::test::make_random_vector<T>(100);
      const auto norm2_a = tulib::math::wrap_norm2(vec_a);
      const auto norm2_b = tulib::math::wrap_norm2(vec_b);
      const auto dot_ab = tulib::math::wrap_dot_product(vec_a, vec_b);

      auto vec_c = vec_a;
      tulib::math::wrap_axpy(vec_c, rand_scalar, vec_b); // c = a + s*b
      const real_t c_ref_norm2 = norm2_a+abs2_s*norm2_b+real_t(2.0)*std::real(rand_scalar*dot_ab);
      CHECK(tulib::math::wrap_norm2(vec_c) == Approx(c_ref_norm2).epsilon(eps*100));

      auto vec_d = vec_a;
      tulib::math::wrap_axpy(vec_d, real_t(-1.0), vec_a);
      CHECK(tulib::math::wrap_norm2(vec_d) == Approx(0.0).margin(eps*10));

      // Create vector of vectors with same data
      std::vector<std::vector<T>> vv_a(20);
      std::vector<std::vector<T>> vv_b(20);
      size_t count=0;
      for(size_t i=0; i<vv_a.size(); ++i)
      {
         vv_a[i].resize(5);
         vv_b[i].resize(5);
         for(size_t j=0; j<vv_a[i].size(); ++j)
         {
            vv_a[i][j] = vec_a[count];
            vv_b[i][j] = vec_b[count];
            ++count;
         }
      }
      tulib::math::wrap_axpy(vv_a, rand_scalar, vv_b);
      CHECK(tulib::math::wrap_norm2(vv_a) == Approx(c_ref_norm2).epsilon(eps*100));

      // Create array of vectors
      detail::five_array<std::vector<T> > fatv_a  =   {  std::vector<T>(vec_a.begin(),    vec_a.begin()+20)
                                                      ,  std::vector<T>(vec_a.begin()+20, vec_a.begin()+40)
                                                      ,  std::vector<T>(vec_a.begin()+40, vec_a.begin()+60)
                                                      ,  std::vector<T>(vec_a.begin()+60, vec_a.begin()+80)
                                                      ,  std::vector<T>(vec_a.begin()+80, vec_a.begin()+100)
                                                      };
      const detail::five_array<std::vector<T> > fatv_b  =   {  std::vector<T>(vec_b.begin(),    vec_b.begin()+20)
                                                            ,  std::vector<T>(vec_b.begin()+20, vec_b.begin()+40)
                                                            ,  std::vector<T>(vec_b.begin()+40, vec_b.begin()+60)
                                                            ,  std::vector<T>(vec_b.begin()+60, vec_b.begin()+80)
                                                            ,  std::vector<T>(vec_b.begin()+80, vec_b.begin()+100)
                                                            };
      tulib::math::wrap_axpy(fatv_a, rand_scalar, fatv_b);
      CHECK(tulib::math::wrap_norm2(fatv_a) == Approx(c_ref_norm2).epsilon(eps*100));

      // Create tuple of scalar, vector, and matrix
      T ts_a = vec_a[0];
      std::vector<T> tv_a(35);
      for(size_t i=0; i<tv_a.size(); ++i)
      {
         tv_a[i] = vec_a[i+1];
      }
      std::vector<std::vector<T>> tm_a(8);
      std::for_each(tm_a.begin(), tm_a.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm_a.size(); ++icol)
      {
         for(size_t irow=0; irow<tm_a[icol].size(); ++irow)
         {
            tm_a[irow][icol] = vec_a[count+36];
            ++count;
         }
      }
      auto tup_a = std::make_tuple(std::move(ts_a), std::move(tv_a), std::move(tm_a));
      T ts_b = vec_b[0];
      std::vector<T> tv_b(35);
      for(size_t i=0; i<tv_b.size(); ++i)
      {
         tv_b[i] = vec_b[i+1];
      }
      std::vector<std::vector<T>> tm_b(8);
      std::for_each(tm_b.begin(), tm_b.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<tm_b.size(); ++icol)
      {
         for(size_t irow=0; irow<tm_b[icol].size(); ++irow)
         {
            tm_b[irow][icol] = vec_b[count+36];
            ++count;
         }
      }
      const auto tup_b = std::make_tuple(std::move(ts_b), std::move(tv_b), std::move(tm_b));
      tulib::math::wrap_axpy(tup_a, rand_scalar, tup_b);
      CHECK(tulib::math::wrap_norm2(tup_a) == Approx(c_ref_norm2).epsilon(eps*100));

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double vd_a(vec_a.size());
         std::copy(vec_a.begin(), vec_a.end(), vd_a.begin());
         detail::vector_double vd_b(vec_b.size());
         std::copy(vec_b.begin(), vec_b.end(), vd_b.begin());
         tulib::math::wrap_axpy(vd_a, rand_scalar, vd_b);
         CHECK(tulib::math::wrap_norm2(vd_a) == Approx(c_ref_norm2).epsilon(eps*100));


         detail::vector_double_private vdp_a(vec_a.size());
         vdp_a.load_in(vec_a);
         detail::vector_double_private vdp_b(vec_b.size());
         vdp_b.load_in(vec_b);
         tulib::math::wrap_axpy(vdp_a, rand_scalar, vdp_b);
         CHECK(tulib::math::wrap_norm2(vdp_a) == Approx(c_ref_norm2).epsilon(eps*100));
      }
   }
};

/**
 * Test the tulib::math::wrap_linear_combination implementation
 **/
struct container_linear_combination_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      // Try wrapper for std::vector
      const auto coefs = tulib::test::make_random_vector<T>(5);
      std::vector<std::vector<T>> vecs(5);
      for(int i=0; i<5; ++i)
      {
         vecs[i] = tulib::test::make_random_vector<T>(100);
      }

      // Do linear combination
      auto lincomb = tulib::math::wrap_linear_combination(vecs, coefs);
      const auto refnorm2 = tulib::math::wrap_norm2(lincomb);

      // Subtract vectors
      for(int i=0; i<5; ++i)
      {
         tulib::math::wrap_axpy(lincomb, -coefs[i], vecs[i]);
      }

      // Check norm
      CHECK(tulib::math::wrap_norm2(lincomb) == Approx(0.0).margin(eps*refnorm2*100));
   }
};

/**
 * Test the tulib::math::wrap_scaled_error implementation
 **/
struct container_scaled_error_test
{
   template <typename T>
   static void run()
   {
      static_assert(std::is_floating_point_v<T> || tulib::meta::is_complex_v<T>);
      using real_t = tulib::meta::real_type_t<T>;
      constexpr auto eps = std::numeric_limits<real_t>::epsilon();

      // Define thresholds
      const real_t abs = 1.e-12;
      const real_t rel = 1.e-14;
      real_t sc = 0;
      real_t aux_err = 0;

      // Try wrapper for tulib::vector (even and odd size to test tiling)
      const long size_even = 100;
      const auto dy_even = tulib::test::make_random_vector<T>(size_even);
      const auto yn_even = tulib::test::make_random_vector<T>(size_even);
      const auto yo_even = tulib::test::make_random_vector<T>(size_even);
      real_t ref_mean_err_even = 0;
      real_t ref_max_err_even = 0;
      for(int i=0; i<size_even; ++i)
      {
         sc = abs + rel*std::max(std::abs(yn_even[i]), std::abs(yo_even[i]));
         aux_err = tulib::math::abs2(dy_even[i] / sc);
         ref_mean_err_even += aux_err;
         ref_max_err_even = std::max(ref_max_err_even, aux_err);
      }
      ref_max_err_even = std::sqrt(ref_max_err_even);
      ref_mean_err_even = std::sqrt(ref_mean_err_even) / size_even;

      CHECK(tulib::math::wrap_scaled_error(dy_even, yn_even, yo_even, abs, rel, false) == Approx(ref_mean_err_even).epsilon(eps*10));
      CHECK(tulib::math::wrap_scaled_error(dy_even, yn_even, yo_even, abs, rel, true) == Approx(ref_max_err_even).epsilon(eps*10));

      const long size_odd = 101;
      const auto dy_odd = tulib::test::make_random_vector<T>(size_odd);
      const auto yn_odd = tulib::test::make_random_vector<T>(size_odd);
      const auto yo_odd = tulib::test::make_random_vector<T>(size_odd);
      real_t ref_mean_err_odd = 0;
      real_t ref_max_err_odd = 0;
      for(int i=0; i<size_odd; ++i)
      {
         sc = abs + rel*std::max(std::abs(yn_odd[i]), std::abs(yo_odd[i]));
         aux_err = tulib::math::abs2(dy_odd[i] / sc);
         ref_mean_err_odd += aux_err;
         ref_max_err_odd = std::max(ref_max_err_odd, aux_err);
      }
      ref_max_err_odd = std::sqrt(ref_max_err_odd);
      ref_mean_err_odd = std::sqrt(ref_mean_err_odd) / size_odd;

      CHECK(tulib::math::wrap_scaled_error(dy_odd, yn_odd, yo_odd, abs, rel, false) == Approx(ref_mean_err_odd).epsilon(eps*10));
      CHECK(tulib::math::wrap_scaled_error(dy_odd, yn_odd, yo_odd, abs, rel, true) == Approx(ref_max_err_odd).epsilon(eps*10));

      // Create vector of vectors with same data
      std::vector<std::vector<T>> dy_vv(20);
      std::vector<std::vector<T>> yn_vv(20);
      std::vector<std::vector<T>> yo_vv(20);
      size_t count=0;
      for(size_t i=0; i<dy_vv.size(); ++i)
      {
         dy_vv[i].resize(5);
         yn_vv[i].resize(5);
         yo_vv[i].resize(5);
         for(size_t j=0; j<dy_vv[i].size(); ++j)
         {
            dy_vv[i][j] = dy_even[count];
            yn_vv[i][j] = yn_even[count];
            yo_vv[i][j] = yo_even[count];
            ++count;
         }
      }
      CHECK(tulib::math::wrap_scaled_error(dy_vv, yn_vv, yo_vv, abs, rel, false) == Approx(ref_mean_err_even).epsilon(eps*10));
      CHECK(tulib::math::wrap_scaled_error(dy_vv, yn_vv, yo_vv, abs, rel, true) == Approx(ref_max_err_even).epsilon(eps*10));

      // Create array of vectors
      detail::five_array<std::vector<T> > dy_fatv  =  {  std::vector<T>(dy_even.begin(),    dy_even.begin()+20)
                                                      ,  std::vector<T>(dy_even.begin()+20, dy_even.begin()+40)
                                                      ,  std::vector<T>(dy_even.begin()+40, dy_even.begin()+60)
                                                      ,  std::vector<T>(dy_even.begin()+60, dy_even.begin()+80)
                                                      ,  std::vector<T>(dy_even.begin()+80, dy_even.begin()+100)
                                                      };
      detail::five_array<std::vector<T> > yn_fatv =   {  std::vector<T>(yn_even.begin(),    yn_even.begin()+20)
                                                      ,  std::vector<T>(yn_even.begin()+20, yn_even.begin()+40)
                                                      ,  std::vector<T>(yn_even.begin()+40, yn_even.begin()+60)
                                                      ,  std::vector<T>(yn_even.begin()+60, yn_even.begin()+80)
                                                      ,  std::vector<T>(yn_even.begin()+80, yn_even.begin()+100)
                                                      };
      detail::five_array<std::vector<T> > yo_fatv =   {  std::vector<T>(yo_even.begin(),    yo_even.begin()+20)
                                                      ,  std::vector<T>(yo_even.begin()+20, yo_even.begin()+40)
                                                      ,  std::vector<T>(yo_even.begin()+40, yo_even.begin()+60)
                                                      ,  std::vector<T>(yo_even.begin()+60, yo_even.begin()+80)
                                                      ,  std::vector<T>(yo_even.begin()+80, yo_even.begin()+100)
                                                      };
      CHECK(tulib::math::wrap_scaled_error(dy_fatv, yn_fatv, yo_fatv, abs, rel, false) == Approx(ref_mean_err_even).epsilon(eps*10));
      CHECK(tulib::math::wrap_scaled_error(dy_fatv, yn_fatv, yo_fatv, abs, rel, true) == Approx(ref_max_err_even).epsilon(eps*10));

      // Create tuple of scalar, vector, and matrix
      T dy_ts = dy_odd[0];
      T yn_ts = yn_odd[0];
      T yo_ts = yo_odd[0];
      std::vector<T> dy_tv(36);
      std::vector<T> yn_tv(36);
      std::vector<T> yo_tv(36);
      for(size_t i=0; i<dy_tv.size(); ++i)
      {
         dy_tv[i] = dy_odd[i+1];
         yn_tv[i] = yn_odd[i+1];
         yo_tv[i] = yo_odd[i+1];
      }
      std::vector<std::vector<T>> dy_tm(8);
      std::for_each(dy_tm.begin(), dy_tm.end(), [](auto& v_) { v_.resize(8); });
      std::vector<std::vector<T>> yn_tm(8);
      std::for_each(yn_tm.begin(), yn_tm.end(), [](auto& v_) { v_.resize(8); });
      std::vector<std::vector<T>> yo_tm(8);
      std::for_each(yo_tm.begin(), yo_tm.end(), [](auto& v_) { v_.resize(8); });
      count = 0;
      for(size_t icol=0; icol<dy_tm.size(); ++icol)
      {
         for(size_t irow=0; irow<dy_tm[icol].size(); ++irow)
         {
            dy_tm[irow][icol] = dy_odd[count+37];
            yn_tm[irow][icol] = yn_odd[count+37];
            yo_tm[irow][icol] = yo_odd[count+37];
            ++count;
         }
      }
      auto dy_tup = std::make_tuple(std::move(dy_ts), std::move(dy_tv), std::move(dy_tm));
      auto yn_tup = std::make_tuple(std::move(yn_ts), std::move(yn_tv), std::move(yn_tm));
      auto yo_tup = std::make_tuple(std::move(yo_ts), std::move(yo_tv), std::move(yo_tm));
      CHECK(tulib::math::wrap_scaled_error(dy_tup, yn_tup, yo_tup, abs, rel, false) == Approx(ref_mean_err_odd).epsilon(eps*10));
      CHECK(tulib::math::wrap_scaled_error(dy_tup, yn_tup, yo_tup, abs, rel, true) == Approx(ref_max_err_odd).epsilon(eps*10));

      // If T=double, we try for non-template type as well
      if constexpr   (  std::is_same_v<T, double>
                     )
      {
         detail::vector_double dy_vd(size_even);
         std::copy(dy_even.begin(), dy_even.end(), dy_vd.begin());
         detail::vector_double yn_vd(size_even);
         std::copy(yn_even.begin(), yn_even.end(), yn_vd.begin());
         detail::vector_double yo_vd(size_even);
         std::copy(yo_even.begin(), yo_even.end(), yo_vd.begin());
         CHECK(tulib::math::wrap_scaled_error(dy_vd, yn_vd, yo_vd, abs, rel, false) == Approx(ref_mean_err_even).epsilon(eps*10));
         CHECK(tulib::math::wrap_scaled_error(dy_vd, yn_vd, yo_vd, abs, rel, true) == Approx(ref_max_err_even).epsilon(eps*10));

         detail::vector_double_private dy_vdp(size_even);
         dy_vdp.load_in(dy_even);
         detail::vector_double_private yn_vdp(size_even);
         yn_vdp.load_in(yn_even);
         detail::vector_double_private yo_vdp(size_even);
         yo_vdp.load_in(yo_even);
         CHECK(tulib::math::wrap_scaled_error(dy_vdp, yn_vdp, yo_vdp, abs, rel, false) == Approx(ref_mean_err_even).epsilon(eps*10));
         CHECK(tulib::math::wrap_scaled_error(dy_vdp, yn_vdp, yo_vdp, abs, rel, true) == Approx(ref_max_err_even).epsilon(eps*10));
      }
   }
};

} /* namespace tulib::test */
