/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file is_detected.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <type_traits>

namespace tulib::meta
{

namespace detail
{

/**
 * Struct for indicating detection failure
 **/
struct none
{
   none() = delete;
   ~none() = delete;
   none(const none&) = delete;
   void operator=(const none&) = delete;
};

/**
 * Detector class
 **/
template
   <  typename Default
   ,  typename AlwaysVoid
   ,  template<typename...> typename Op
   ,  typename... Args
   >
struct detector
{
   using value_t = std::false_type;
   using type = Default;
};

/**
 * Specialization for successful detections
 **/
template
   <  typename Default
   ,  template<typename...> typename Op
   ,  typename... Args
   >
struct detector
   <  Default
   ,  std::void_t<Op<Args...>>   // This only works if Op<Args...> exists. Otherwise, we fall back to default (false_type).
   ,  Op
   ,  Args...
   >
{
   using value_t = std::true_type;
   using type = Op<Args...>;
};

/**
 * is_detected
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
using is_detected = typename detector<none, void, Op, Args...>::value_t;

} /* namespace detail */

/**
 * Is Op<Args...> detected. True or false?
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
inline constexpr bool is_detected_v = detail::is_detected<Op, Args...>::value;

/**
 * Detected type
 **/
template
   <  template<typename...> typename Op
   ,  typename... Args
   >
using detected_t = typename detail::detector<detail::none, void, Op, Args...>::type;

} /* namespace tulib::meta */
