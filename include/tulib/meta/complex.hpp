/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file complex.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <type_traits>
#include <complex>

namespace tulib::meta
{
namespace detail
{
/**
 * Struct to determine if a template is a complex number
 **/
//!@{
template <typename T>
struct is_complex
   :  public std::false_type
{
};
template <typename T>
struct is_complex<std::complex<T> >
   :  public std::true_type
{
   static_assert(std::is_floating_point_v<T>, "T must be floating-point type.");
};
//!@}

/**
 * Determine T in std::complex<T>
 **/
//!@{
template <typename T>
struct real_type
{
   static_assert(std::is_floating_point_v<T>, "T must be floating-point type.");
   using type = T;
};
template <typename T>
struct real_type<std::complex<T> >
{
   static_assert(std::is_floating_point_v<T>, "T must be floating-point type.");
   using type = T;
};
//!@}

} /* namespace detail */

/**
 * Helpers for is_complex
 **/
template <typename T> inline constexpr bool is_complex_v = detail::is_complex<T>::value;
template <typename T> inline constexpr bool is_number_v = is_complex_v<T> || std::is_floating_point_v<T>;


/**
 * Helper for real_type
 **/
template <typename T>
using real_type_t = typename detail::real_type<T>::type;

} /* namespace midas::type_traits */
