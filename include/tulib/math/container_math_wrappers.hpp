/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file container_math_wrappers.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <numeric>
#include <algorithm>

#ifdef TULIB_PAR_STL
#include <execution>
#endif /* TULIB_PAR_STL */

#include <tulib/math/core.hpp>
#include <tulib/math/range_operations.hpp>
#include <tulib/meta/template.hpp>
#include <tulib/meta/is_detected.hpp>
#include <tulib/meta/is_iterable.hpp>
#include <tulib/meta/is_tuple.hpp>
#include <tulib/meta/static_loop.hpp>
#include <tulib/util/exceptions.hpp>

namespace tulib::math
{

/**
 * Size wrapper
 **/
//!@{
namespace detail
{
template<typename T> using has_size_func_type = decltype(size(std::declval<T>()));
template<typename T> using has_Size_func_type = decltype(Size(std::declval<T>()));
template<typename T> using has_size_member_type = decltype(std::declval<T>().size());
template<typename T> using has_Size_member_type = decltype(std::declval<T>().Size());
} /* namespace detail */

//! Size of numbers (should only be called when there is no container. otherwise inefficient)
template
   <  typename T
   ,  std::enable_if_t<tulib::meta::is_number_v<T> >* = nullptr
   >
inline constexpr std::size_t wrap_size
   (  const T&
   )
{
   return 1;
}

//! Size
template
   <  typename T
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   >
std::size_t wrap_size
   (  const T& c_
   )
{
   // If we have an iterable template (e.g. vector of vectors), we must take extra care.
   if constexpr   (  tulib::meta::is_template_v<T>
                  && !tulib::meta::is_tuple_v<T>
                  && tulib::meta::is_iterable_v<T>
                  )
   {
      using std::begin; using std::end;
      using value_t = std::decay_t<typename std::iterator_traits<decltype(begin(c_))>::value_type>;

      // If the template parameter is not floating-point or complex, accumulate individual sizes.
      if constexpr   (  !tulib::meta::is_number_v<value_t>
                     )
      {
         return std::accumulate
            (  begin(c_)
            ,  end(c_)
            ,  0
            ,  [](std::size_t sum_, const auto& i_) -> std::size_t { return sum_ + wrap_size(i_); }
            );
      }
      // Else, move on to the following checks.
   }

   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_size_func_type, T>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_Size_func_type, T>;
   constexpr bool has_member_impl = tulib::meta::is_detected_v<detail::has_size_member_type, T>;
   constexpr bool has_Member_impl = tulib::meta::is_detected_v<detail::has_Size_member_type, T>;

   if constexpr   (  has_func_impl
                  )
   {
      return size(c_);
   }
   else if constexpr (  has_Func_impl
                     )
   {
      return Size(c_);
   }
   else if constexpr (  has_member_impl
                     )
   {
      return c_.size();
   }
   else if constexpr (  has_Member_impl
                     )
   {
      return c_.Size();
   }
   else if constexpr (  tulib::meta::is_tuple_v<T>
                     )
   {
      std::size_t s = 0;

      tulib::meta::static_for<0,std::tuple_size_v<T>>::loop
         (  [&c_, &s](auto I)
               {  s += wrap_size(std::get<I>(c_));
               }
         );

      return s;
   }
   else
   {
      static_assert(tulib::meta::is_iterable_v<T>, "Template is not iterable. Cannot get total size!");

      using std::begin;
      using std::end;
      using value_t = std::decay_t<typename std::iterator_traits<decltype(begin(c_))>::value_type>;

      // If the template parameter is floating-point or complex, return distance between begin and end
      if constexpr   (  tulib::meta::is_number_v<value_t>
                     )
      {
         using std::distance;
         auto beg = begin(c_);
         auto e = end(c_);

         return distance(beg, e);
      }
      // Else, sum sizes
      else
      {
         return std::accumulate
            (  begin(c_)
            ,  end(c_)
            ,  0
            ,  [](std::size_t sum_, const auto& i_) -> std::size_t { return sum_ + wrap_size(i_); }
            );
      }
   }
}
//!@}

/**
 * Scaling wrapper
 **/
//!@{
namespace detail
{
template<typename V, typename W> using has_scale_func_type = decltype(scale(std::declval<V&>(), std::declval<W>()));
template<typename V, typename W> using has_Scale_func_type = decltype(Scale(std::declval<V&>(), std::declval<W>()));
template<typename V, typename W> using has_scale_member_type = decltype(std::declval<V>().scale(std::declval<W>()));
template<typename V, typename W> using has_Scale_member_type = decltype(std::declval<V>().Scale(std::declval<W>()));
template<typename V, typename W> using has_operator_type = decltype(std::declval<V>() *= std::declval<W>());
} /* namespace detail */


//! Scale for numbers
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<tulib::meta::is_number_v<T> >* = nullptr
   ,  std::enable_if_t<tulib::meta::is_number_v<U> >* = nullptr
   >
inline void wrap_scale
   (  T& v_
   ,  const U& scal_
   )
{
   static_assert(!(std::is_floating_point_v<T> && tulib::meta::is_complex_v<U>), "Cannot scale a real number by a complex one!");
   v_ *= scal_;
}

//! Scale
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   ,  std::enable_if_t<tulib::meta::is_number_v<U> >* = nullptr
   >
void wrap_scale
   (  T& c_
   ,  const U& scal_
   )
{
   // Check if T implements scale
   constexpr bool has_member_impl = tulib::meta::is_detected_v<detail::has_scale_member_type, T, U>;
   constexpr bool has_Member_impl = tulib::meta::is_detected_v<detail::has_Scale_member_type, T, U>;

   // Check if T implements operator*=
   constexpr bool has_operator_impl = tulib::meta::is_detected_v<detail::has_operator_type, T, U>;

   // Check for scale(T, U) in global namespace
   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_scale_func_type, T, U>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_Scale_func_type, T, U>;

   // Call implementation
   if constexpr   (  has_func_impl
                  )
   {
      scale(c_, scal_);
      return;
   }
   else if constexpr (  has_Func_impl
                     )
   {
      Scale(c_, scal_);
      return;
   }
   else if constexpr (  has_member_impl
                     )
   {
      c_.scale(scal_);
      return;
   }
   else if constexpr (  has_Member_impl
                     )
   {
      c_.Scale(scal_);
      return;
   }
   else if constexpr (  has_operator_impl
                     )
   {
      c_ *= scal_;
      return;
   }
   else if constexpr (  tulib::meta::is_tuple_v<T>
                     )
   {
      std::apply
         (  [&scal_](auto&&... args)
               {  (  (wrap_scale(args, scal_))
                  ,  ...
                  );
               }
         ,  c_
         );
      return;
   }
   // Else, use range iterator
   else
   {
      using std::begin; using std::end;
#ifdef TULIB_PAR_STL /** TODO: Set global constexpr bool - and only use par at first layer if nested template! **/
      std::for_each
         (  std::execution::par_unseq
         ,  begin(c_), end(c_)
         ,  [&scal_](auto& i_)
            {
               wrap_scale(i_, scal_);
            }
         );
#else
      std::for_each
         (  begin(c_), end(c_)
         ,  [&scal_](auto& i_)
            {
               wrap_scale(i_, scal_);
            }
         );
#endif /* TULIB_PAR_STL */
      return;
   }
}

//!@}



/**
 * Axpy wrapper
 **/
//!@{
namespace detail
{
template<typename V, typename W> using has_axpy_member_type = decltype(std::declval<V>().axpy(std::declval<W>(), std::declval<const V&>()));
template<typename V, typename W> using has_Axpy_member_type = decltype(std::declval<V>().Axpy(std::declval<W>(), std::declval<const V&>()));
template<typename V, typename W> using has_axpy_func_type = decltype(axpy(std::declval<V&>(), std::declval<W>(), std::declval<const V&>()));
template<typename V, typename W> using has_Axpy_func_type = decltype(Axpy(std::declval<V&>(), std::declval<W>(), std::declval<const V&>()));
} /* namespace detail */


//! axpy for numbers
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<tulib::meta::is_number_v<T> >* = nullptr
   ,  std::enable_if_t<tulib::meta::is_number_v<U> >* = nullptr
   >
inline void wrap_axpy
   (  T& a_
   ,  const U& scal_
   ,  const T& b_
   )
{
   a_ += scal_ * b_;
}

//! axpy
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   ,  std::enable_if_t<tulib::meta::is_number_v<U> >* = nullptr
   >
void wrap_axpy
   (  T& a_
   ,  const U& scal_
   ,  const T& b_
   )
{
   // Check if T implements axpy
   constexpr bool has_member_impl = tulib::meta::is_detected_v<detail::has_axpy_member_type, T, U>;
   constexpr bool has_Member_impl = tulib::meta::is_detected_v<detail::has_Axpy_member_type, T, U>;

   // Check for axpy(T, U, T) in global namespace
   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_axpy_func_type, T, U>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_Axpy_func_type, T, U>;

   // Call implementation
   if constexpr   (  has_func_impl
                  )
   {
      axpy(a_, scal_, b_);
      return;
   }
   else if constexpr (  has_Func_impl
                     )
   {
      Axpy(a_, scal_, b_);
      return;
   }
   else if constexpr (  has_member_impl
                     )
   {
      a_.axpy(scal_, b_);
      return;
   }
   else if constexpr (  has_Member_impl
                     )
   {
      a_.Axpy(scal_, b_);
      return;
   }
   else if constexpr (  tulib::meta::is_tuple_v<T>
                     )
   {
      tulib::meta::static_for<0,std::tuple_size_v<T> >::loop
         (  [&a_,&scal_,&b_](auto I)
               {  wrap_axpy(std::get<I>(a_), scal_, std::get<I>(b_));
               }
         );
      return;
   }
   // Else, use range iterator
   else
   {
      static_assert(tulib::meta::is_iterable_v<T>);
      // Allow for custom begin, end, advance by using ADL
      using std::begin;
      using std::end;
      using std::advance;

      auto beg = begin(a_);
      auto e = end(a_);
      auto beg_other = begin(b_);
      auto len = std::distance(beg, e);
      using value_t = typename std::iterator_traits<decltype(beg)>::value_type;

      if constexpr ( tulib::meta::is_number_v<value_t> )
      {
         // TODO: Impl a parallel algorithm.
         for(; beg != e; advance(beg, 1), advance(beg_other, 1))
         {
            wrap_axpy(*beg, scal_, *beg_other);
         }
      }
      else
      {
         std::vector<std::pair<std::reference_wrapper<value_t>, std::reference_wrapper<const value_t>>> refs;
         refs.reserve(len);
         // Enable parallel STL. Make vector of pairs of reference wrappers and then run std::for_each.
         // NB: Shouldn't do this if value_t is a number!!!
         for(; beg != e; advance(beg, 1), advance(beg_other, 1))
         {
            refs.emplace_back(std::ref(*beg), std::cref(*beg_other));
         }

         std::for_each
#ifdef    TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  refs.begin(), refs.end()
#else
         (  refs.begin(), refs.end()
#endif    /* TULIB_PAR_STL */
         ,  [&scal_](auto& x_)
            {
               wrap_axpy(x_.first.get(), scal_, x_.second.get());
            }
         );
      }
      return;
   }
}

//!@}


/**
 * Dot-product wrapper
 **/
//!@{
namespace detail
{
template<typename V> using has_dot_member_type = decltype(std::declval<V>().dot(std::declval<V>()));
template<typename V> using has_Dot_member_type = decltype(std::declval<V>().Dot(std::declval<V>()));
template<typename V> using has_dot_product_func_type = decltype(dot_product(std::declval<V>(), std::declval<V>()));
template<typename V> using has_Dot_func_type = decltype(Dot(std::declval<V>(), std::declval<V>()));
} /* namespace detail */

//! dot_product
template
   <  typename T
   ,  std::enable_if_t<tulib::meta::is_number_v<T> >* = nullptr
   >
inline constexpr T wrap_dot_product
   (  const T& a_
   ,  const T& b_
   )
{
   return tulib::math::conj(a_) * b_;
}

//! dot for non-template container
template
   <  typename T
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   >
auto wrap_dot_product
   (  const T& a_
   ,  const T& b_
   )
{
   // Check if T implements dot
   constexpr bool has_member_impl = tulib::meta::is_detected_v<detail::has_dot_member_type, const T&>;
   constexpr bool has_Member_impl = tulib::meta::is_detected_v<detail::has_Dot_member_type, const T&>;

   // Check for dot_product(T, T) in global namespace
   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_dot_product_func_type, const T&>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_Dot_func_type, const T&>;

   // Call implementation
   if constexpr   (  has_func_impl
                  )
   {
      return dot_product(a_, b_);
   }
   else if constexpr (  has_Func_impl
                     )
   {
      return Dot(a_, b_);
   }
   else if constexpr (  has_member_impl
                     )
   {
      return a_.dot(b_);
   }
   else if constexpr (  has_Member_impl
                     )
   {
      return a_.Dot(b_);
   }
   else if constexpr (  tulib::meta::is_tuple_v<T>
                     )
   {
      using first_t = typename std::tuple_element_t<0, T>;

      using dot_t = decltype(wrap_dot_product(std::declval<first_t>(), std::declval<first_t>()));

      dot_t result = 0;

      tulib::meta::static_for<0,std::tuple_size_v<T> >::loop
         (  [&a_,&b_,&result](auto I)
            {  result += wrap_dot_product(std::get<I>(a_), std::get<I>(b_));
            }
         );

      return result;
   }
   // Else, use range iterator
   else
   {
      static_assert(tulib::meta::is_iterable_v<T>);

      // Allow for custom begin by using ADL
      using std::begin; using std::end;
      using value_t = typename std::iterator_traits<decltype(begin(a_))>::value_type;
      using dot_t = decltype(wrap_dot_product(std::declval<value_t>(), std::declval<value_t>()));
      static_assert(tulib::meta::is_number_v<dot_t>);
      // NKM: Tests show that for double std::transform_reduce and tulib::dot_product_impl are faster than std::inner_product.
      // For complex numbers, the performance of the three is comparable, but the STL variants are parallelizable.
      return std::transform_reduce
#ifdef TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  begin(a_), end(a_)
#else
         (  begin(a_), end(a_)
#endif /* TULIB_PAR_STL */
         ,  begin(b_)
         ,  dot_t(0.0)
         ,  std::plus<>()
         ,  [](const auto& i_, const auto& j_)
            {
               return wrap_dot_product(i_, j_);
            }
         );
   }
}

//!@}

/**
 * Norm2 wrapper
 **/
//!@{
namespace detail
{
template<typename U> using has_norm2_member_type = decltype(std::declval<U>().norm2());
template<typename U> using has_Norm2_member_type = decltype(std::declval<U>().Norm2());
template<typename U> using has_norm2_func_type = decltype(norm2(std::declval<U>()));
template<typename U> using has_Norm2_func_type = decltype(Norm2(std::declval<U>()));
} /* namespace detail */

//! squared norm for numbers
template
   <  typename T
   ,  std::enable_if_t<tulib::meta::is_number_v<T> >* = nullptr
   >
inline constexpr typename tulib::meta::real_type_t<T> wrap_norm2
   (  const T& x_
   )
{
   return tulib::math::abs2(x_);
}

//! squared norm for non-template containers
template
   <  typename T
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   >
auto wrap_norm2
   (  const T& c_
   )
{
   // Check if T implements norm2
   constexpr bool has_member_impl = tulib::meta::is_detected_v<detail::has_norm2_member_type, T>;
   constexpr bool has_Member_impl = tulib::meta::is_detected_v<detail::has_Norm2_member_type, T>;

   // Check for norm2(T) in global namespace
   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_norm2_func_type, T>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_Norm2_func_type, T>;

   // Call implementation
   if constexpr   (  has_func_impl
                  )
   {
      return norm2(c_);
   }
   else if constexpr (  has_Func_impl
                     )
   {
      return Norm2(c_);
   }
   else if constexpr (  has_member_impl
                     )
   {
      return c_.norm2();
   }
   else if constexpr (  has_Member_impl
                     )
   {
      return c_.Norm2();
   }
   // Else, use range iterator
   else if constexpr (  tulib::meta::is_tuple_v<T>
                     )
   {
      using first_t = typename std::tuple_element<0, T>::type;
      using ret_t = decltype(wrap_norm2(std::declval<first_t>()));
      ret_t result = 0;
      tulib::meta::static_for<0,std::tuple_size_v<T> >::loop
         (  [&c_,&result](auto I)
               {  result += wrap_norm2(std::get<I>(c_));
               }
         );
      return result;
   }
   // Else, compute
   else
   {
      using std::begin; using std::end;
      using value_t = typename std::iterator_traits<decltype(begin(c_))>::value_type;
      using ret_t = decltype(wrap_norm2(std::declval<value_t>()));

      return std::transform_reduce
#ifdef TULIB_PAR_STL
         (  std::execution::par_unseq
         ,  begin(c_), end(c_)
#else
         (  begin(c_), end(c_)
#endif /* TULIB_PAR_STL */
         ,  ret_t(0.0)
         ,  std::plus<>()
         ,  [](const auto& i_)
            {
               return wrap_norm2(i_);
            }
         );
   }
}

//! norm
template
   <  typename T
   >
inline auto wrap_norm
   (  const T& x_
   )
{
   return std::sqrt(wrap_norm2(x_));
}

//!@}

/**
 * Linear-combination
 * Function taking container of vectors and container of scalars
 **/
//!@{
namespace detail
{
template<typename V, typename W> using has_linear_combination_func_type = decltype(linear_combination(std::declval<V>(), std::declval<W>()));
template<typename V, typename W> using has_LinearCombination_func_type = decltype(LinearCombination(std::declval<V>(), std::declval<W>()));
} /* namespace detail */

//! linear combination
template
   <  typename T
   ,  typename U
   ,  std::enable_if_t<!tulib::meta::is_number_v<T> >* = nullptr
   ,  std::enable_if_t<!tulib::meta::is_number_v<U> >* = nullptr
   >
auto wrap_linear_combination
   (  const T& vectors_
   ,  const U& coefs_
   )
{
   // Check for linear_combination(T, U) in global namespace
   constexpr bool has_func_impl = tulib::meta::is_detected_v<detail::has_linear_combination_func_type, T, U>;
   constexpr bool has_Func_impl = tulib::meta::is_detected_v<detail::has_LinearCombination_func_type, T, U>;

   // Call implementation
   if constexpr   (  has_func_impl
                  )
   {
      return linear_combination(vectors_, coefs_);
   }
   else if constexpr (  has_Func_impl
                     )
   {
      return LinearCombination(vectors_, coefs_);
   }
   // Require that both containers are iterable
   else
   {
      static_assert(tulib::meta::is_iterable_v<T>, "Container holding vectors must be iterable");
      static_assert(tulib::meta::is_iterable_v<U>, "Container holding coefficients must be iterable");

      // Allow for custom begin by using ADL
      using std::begin; using std::end; using std::distance;

      auto vbeg = begin(vectors_);
      auto vend = end(vectors_);
      auto cbeg = begin(coefs_);
      auto cend = end(coefs_);
      auto vlen = distance(vbeg, vend);
      auto clen = distance(cbeg, cend);
      if ( vlen != clen )
      {
         TULIB_EXCEPTION("Size of vectors and coefficients mismatch.");
      }

      auto result = *(vbeg++);
      wrap_scale(result, *(cbeg++));
      for(; vbeg != vend; ++vbeg, ++cbeg)
      {
         wrap_axpy(result, *cbeg, *vbeg);
      }

      return result;
   }
}

//!@}

/**
 * Scaled-error (for ODE) wrapper
 **/
//!@{
namespace detail
{
template<typename V, typename ABS_T> using has_scaled_error2_func_type = decltype(scaled_error2(std::declval<V>(), std::declval<V>(), std::declval<V>(), std::declval<ABS_T>(), std::declval<ABS_T>(), std::declval<bool>()));
template<typename V, typename ABS_T> using has_ScaledError2_func_type = decltype(ScaledError2(std::declval<V>(), std::declval<V>(), std::declval<V>(), std::declval<ABS_T>(), std::declval<ABS_T>(), std::declval<bool>()));

//! scaled_error2
template
   <  typename PARAM_T
   ,  typename ABS_T
   ,  std::enable_if_t<std::is_floating_point_v<PARAM_T> || tulib::meta::is_complex_v<PARAM_T> >* = nullptr
   ,  std::enable_if_t<std::is_floating_point_v<ABS_T> >* = nullptr
   >
inline ABS_T wrap_scaled_error2
   (  const PARAM_T& aDeltaY
   ,  const PARAM_T& aYNew
   ,  const PARAM_T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool
   )
{
   auto sc = aAbs + aRel*std::max(std::abs(aYNew), std::abs(aYOld));
   return tulib::math::abs2(aDeltaY / sc);
}

//! Scaled error for non-template containers
template
   <  typename T
   ,  typename ABS_T
   ,  std::enable_if_t<!std::is_floating_point_v<T> >* = nullptr
   ,  std::enable_if_t<!tulib::meta::is_template_v<T> >* = nullptr
   ,  std::enable_if_t<std::is_floating_point_v<ABS_T> >* = nullptr
   >
ABS_T wrap_scaled_error2
   (  const T& aDeltaY
   ,  const T& aYNew
   ,  const T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   )
{
   using ret_t = ABS_T;
   using cont_t = T;
   constexpr bool has_impl = tulib::meta::is_detected_v<detail::has_scaled_error2_func_type, cont_t, ABS_T>;
   constexpr bool has_Impl = tulib::meta::is_detected_v<detail::has_ScaledError2_func_type, cont_t, ABS_T>;

   if constexpr   (  has_impl
                  )
   {
      return scaled_error2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
   }
   else if constexpr (  has_Impl
                     )
   {
      return ScaledError2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
   }
   else
   {
      static_assert(tulib::meta::is_iterable_v<T>);
      return tulib::math::detail::scaled_error2_impl<ret_t, cont_t>(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
   }
}

//! Forward declare scaled_error2 for tuples (and std::pair)
template
   <  typename T
   ,  typename ABS_T
   ,  std::enable_if_t<tulib::meta::is_tuple_v<T>>* = nullptr
   >
ABS_T wrap_scaled_error2
   (  const T& aDeltaY
   ,  const T& aYNew
   ,  const T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   );

//! Scaled error for template containers
template
   <  typename PARAM_T
   ,  typename ABS_T
   ,  template<typename...> typename C
   ,  std::enable_if_t<!tulib::meta::is_complex_v<C<PARAM_T> > >* = nullptr
   ,  std::enable_if_t<std::is_floating_point_v<ABS_T> >* = nullptr
   >
ABS_T wrap_scaled_error2
   (  const C<PARAM_T>& aDeltaY
   ,  const C<PARAM_T>& aYNew
   ,  const C<PARAM_T>& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   )
{
   using ret_t = ABS_T;
   using cont_t = C<PARAM_T>;
   constexpr bool has_impl = tulib::meta::is_detected_v<detail::has_scaled_error2_func_type, cont_t, ABS_T>;
   constexpr bool has_Impl = tulib::meta::is_detected_v<detail::has_ScaledError2_func_type, cont_t, ABS_T>;

   if constexpr   (  has_impl
                  )
   {
      return scaled_error2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
   }
   else if constexpr (  has_Impl
                     )
   {
      return ScaledError2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
   }
   else
   {
      static_assert(tulib::meta::is_iterable_v<C<PARAM_T>>);
      if constexpr   (  std::is_floating_point_v<PARAM_T>
                     || tulib::meta::is_complex_v<PARAM_T>
                     )
      {
         return tulib::math::detail::scaled_error2_impl<ret_t, cont_t>(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax);
      }
      else
      {
         using std::begin;
         using std::end;
         using std::advance;
         using std::distance;

         auto it_dy = begin(aDeltaY);
         auto it_yn = begin(aYNew);
         auto it_yo = begin(aYOld);
         auto end_dy = end(aDeltaY);

         ret_t err2 = 0;

         if (  aMax
            )
         {
            for(; it_dy != end_dy; advance(it_dy, 1), advance(it_yn, 1), advance(it_yo, 1))
            {
               err2 = std::max(err2, wrap_scaled_error2(*it_dy, *it_yn, *it_yo, aAbs, aRel, aMax));
            }
         }
         else
         {
            for(; it_dy != end_dy; advance(it_dy, 1), advance(it_yn, 1), advance(it_yo, 1))
            {
               err2 += wrap_scaled_error2(*it_dy, *it_yn, *it_yo, aAbs, aRel, aMax);
            }
         }

         return err2;
      }
   }
}

//! scaled_error2 for tuples (and std::pair)
template
   <  typename T
   ,  typename ABS_T
   ,  std::enable_if_t<tulib::meta::is_tuple_v<T>>*
   >
ABS_T wrap_scaled_error2
   (  const T& aDeltaY
   ,  const T& aYNew
   ,  const T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   )
{
   using ret_t = ABS_T;

   ret_t result = 0;

   tulib::meta::static_for<0,std::tuple_size_v<T> >::loop
      (  [&](auto I)
            {  auto err2 = wrap_scaled_error2(std::get<I>(aDeltaY), std::get<I>(aYNew), std::get<I>(aYOld), aAbs, aRel, aMax);
               result = aMax ? std::max(result, err2) : result + err2;
            }
      );

   return result;
}

} /* namespace detail */

/**
 * scaled error
 * @note If aMax is false, the error is divided by the size of the vector to get the mean error.
 **/
template
   <  typename T
   ,  typename ABS_T
   >
auto wrap_scaled_error
   (  const T& aDeltaY
   ,  const T& aYNew
   ,  const T& aYOld
   ,  ABS_T aAbs
   ,  ABS_T aRel
   ,  bool aMax
   )
{
   // Max error
   if (  aMax
      )
   {
      return std::sqrt(detail::wrap_scaled_error2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax));
   }
   // Mean error
   else
   {
      return std::sqrt(detail::wrap_scaled_error2(aDeltaY, aYNew, aYOld, aAbs, aRel, aMax)) / wrap_size(aDeltaY);
   }
}
//!@}

} /* namespace tulib::math */
