/**********************************************************************************************
 * This file is a part of tulib - a collection of C++17 template utilities.
 * @file function_wrapper.hpp
 * @author Niels Kristian Kjærgård Madsen
 *
 * MIT License
 * Copyright (c) 2019 Niels Kristian Kjærgård Madsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **********************************************************************************************/

#pragma once

#include <type_traits>

#include <tulib/meta/function_traits.hpp>

namespace tulib::util
{

namespace detail
{
/**
 * Wrapper class for storing functions.
 **/
template
   <  typename F
   ,  bool WrapReference = false
   >
class function_wrapper_impl
{
   public:
      //! Deduce return and argument types
      using function_type = F;
      using return_t = typename tulib::meta::return_type<F>;
      using arg_ts = typename tulib::meta::arguments_tuple_t<F>;
      static constexpr size_t argc = tulib::meta::argument_count<F>;

      //! c-tor
      function_wrapper_impl
         (  F f_
         )  noexcept
         :  _f(std::move(f_))
      {
      }

      //! Invoke function.
      template<typename... Args>
      auto operator()(Args&&... args_)
      {
          return this->_f(std::forward<Args>(args_)...);
      }

   protected:
      //! Get the function object
      F& get() noexcept { return this->_f; }
      const F& get() const noexcept { return this->_f; }

   private:
      //! Hold a function object
      F _f;
};

/**
 * If F is an lvalue reference, we only store the reference
 **/
template
   <  typename F
   >
class function_wrapper_impl<F, true>
{
   public:
      //! Deduce return and argument types
      using function_type = F;
      using return_t = typename tulib::meta::return_type<F>;
      using arg_ts = typename tulib::meta::arguments_tuple_t<F>;
      static constexpr size_t argc = tulib::meta::argument_count<F>;

      //! c-tor
      function_wrapper_impl
         (  F& f_
         )  noexcept
         :  _f(f_)
      {
      }

      //! Invoke function.
      template<typename... Args>
      auto operator()(Args&&... args_)
      {
          return this->_f(std::forward<Args>(args_)...);
      }

   protected:
      //! Get the function object
      F& get() noexcept { return this->_f; }
      const F& get() const noexcept { return this->_f; }

   private:
      //! Hold a reference to the function object
      F& _f;
};

} /* namespace detail */

//! Alias for getting the correct implementation
template<typename F>
using function_wrapper = detail::function_wrapper_impl<std::decay_t<F>, std::is_lvalue_reference_v<F>>;

//! Construct wrapper
template<typename F>
auto wrap_function
   (  F&& f_
   )  noexcept
{
   return function_wrapper<F>(std::forward<F>(f_));
}

} /* namespace tulib::util */
